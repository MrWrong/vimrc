# My .vimrc

This is my no-plugins vimrc, heavily inspired on https://github.com/changemewtf/no_plugins

To use it, clone it somewhere in a projects directory, say, ~/Projects/ and then symlink the files:

```
$ cd
$ ln -s ~/Projects/vimrc/.vim . 
$ ln -s ~/Projects/vimrc/.vimrc . 
```

Now if you start vim, you should see hybrid line numbering. There are snippet examples and there is more configuration,
such as a terminal and a file browser window. Look at the files in .vim to see how to use them.

Enjoy.

