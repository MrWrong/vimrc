" This is my personal .vimrc, heavily based on https://github.com/changemewtf/no_plugins
"  
" https://codeberg.org/thefoggiest/vimrc

execute 'runtime!' 'vimrc.d/*'
